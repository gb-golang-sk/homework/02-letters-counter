package main

import (
	"fmt"
	"unicode"
)

func main() {

	str := "Hello, world!"
	lettersCountsMap := make(map[rune]int)
	lettersCount := 0

	for _, char := range str {
		if unicode.IsLetter(char) {
			lettersCountsMap[unicode.ToLower(char)]++
			lettersCount++
		}
	}

	for key, value := range lettersCountsMap {
		fmt.Printf("%c - %d %0.2f\n", key, value, float32(value)/float32(lettersCount))
	}
}
